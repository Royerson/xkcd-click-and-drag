extends Node2D


func _ready():
	var dir := Directory.new()
	# warning-ignore:return_value_discarded
	dir.open("res://clickdrag/")
	# warning-ignore:return_value_discarded
	dir.list_dir_begin(true)
	var file_name := dir.get_next()
	while file_name:
		if file_name.get_extension() == "import":
			make_sprite( file_name.get_basename() )
		file_name = dir.get_next()


func make_sprite(name:String):
	#name is asdf.png
	var tex:Texture = load("res://clickdrag/" + name)
	tex.flags = 5
	var sprite := Sprite.new()
	sprite.texture = tex
	sprite.centered = false
	sprite.position = nsew_to_chunk( name.get_basename() )
	sprite.position *= 2048
	add_child(sprite)


#will probably crash if given bad input
func nsew_to_chunk(nsew:String) -> Vector2:
	var regex := RegEx.new()
	# warning-ignore:return_value_discarded
	regex.compile("(\\d+)(\\w)(\\d+)(\\w)")
	var matches := regex.search(nsew).strings
	var out := Vector2( matches[3], matches[1] )
	var dirs := [ matches[4], matches[2] ]
	for axis in 2:
		if dirs[axis] == "s" or dirs[axis] == "e":
			out[axis] -= 1
		else:
			out[axis] = -out[axis]
	return out
