extends Camera2D


onready var background = $background/Sprite
onready var b_zoom = $CanvasLayer/GridContainer/Button_zoom
onready var b_back = $CanvasLayer/GridContainer/Button_back

const zoom_factor = pow(2, 1.0/6) #6th root, will double zoom in 6 steps
const zoom_factor_b = pow(2, 1.0/2)

var gui_visible = preload("res://icons/GuiVisibilityVisible.svg")
var gui_hidden = preload("res://icons/GuiVisibilityHidden.svg")

func _ready():
	background.scale = OS.window_size / 64
	update_background()
	# warning-ignore:return_value_discarded
	get_tree().root.connect("size_changed", self, "_on_window_size_changed")


func _on_window_size_changed():
	background.scale = OS.window_size / 64
	update_background()
	fix_blur()


func _input(event):
	if event is InputEventMouseMotion and Input.get_mouse_button_mask():
		position -= event.relative * zoom
		update_background()
	
	if event is InputEventMouseButton and event.pressed:
		if event.button_index == BUTTON_WHEEL_UP:
			change_zoom(zoom_factor)
		if event.button_index == BUTTON_WHEEL_DOWN:
			change_zoom(zoom_factor, false)


func reset_zoom():
	zoom = Vector2.ONE
	fix_blur()
	set_zoom_button_text()
	update_background()


func change_zoom(factor:float, zoom_in:=true):
	if zoom_in:
		zoom /= factor
	else:
		zoom *= factor
	zoom = vector_max(zoom, 1.0/8) #800%
	zoom = vector_min(zoom, 256) #~0.4%
	fix_blur()
	set_zoom_button_text()
	update_background()


func fix_blur():
	if zoom != Vector2.ONE:
		return
	position = position.floor()
	#fix blurriness from odd sized camera
	for axis in 2:
		if int(OS.window_size[axis]) % 2:
			position[axis] += 0.5


func set_zoom_button_text():
	var out := 1/zoom.x
	out *= 100
	out = stepify(out, 0.1)
	b_zoom.text = str(out) + "%"


func update_background():
	var shad := 0.5
	shad -= (1000 + position.y) / (OS.window_size.y * zoom.y)
	background.material.set_shader_param("split", shad)


func vector_min(a:Vector2, b:float) -> Vector2:
	for axis in 2:
		a[axis] = min(a[axis], b)
	return a
func vector_max(a:Vector2, b:float) -> Vector2:
	for axis in 2:
		a[axis] = max(a[axis], b)
	return a


func _on_Button_zoom_pressed():
	reset_zoom()


func _on_Button_more_pressed():
	change_zoom(zoom_factor_b)


func _on_Button_less_pressed():
	change_zoom(zoom_factor_b, false)


func _on_Button_back_pressed():
	background.visible = !background.visible
	if background.visible:
		b_back.icon = gui_visible
	else:
		b_back.icon = gui_hidden
